import React, { Component } from 'react';
import { Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import AuthRoute from '../../components/auth-route';
import Header from '../../components/header';
import Spinner from '../../components/spinner';
import Toasts from '../../containers/toasts';

import Landing from '../../pages/landing';
import Home from '../../pages/home';
import Article from '../../pages/article';

import * as routes from '../../../app/routes';

import { authActions } from '../../../redux/auth';

class Root extends Component {
	componentDidMount() {
		if (!this.props.initialized) {
			return this.props.initialize();
		}
	}

	render() {
		if (this.props.initializing) {
			return <Spinner fullscreen size={160} />;
		}

		return (
			<>
				<Header />
				<Switch>
					<AuthRoute exact isPublic name="Landing" path={routes.LANDING} render={() => <Landing route={routes.LANDING} />} />
					<AuthRoute isPublic name="Register" path={routes.REGISTER} render={() => <Landing route={routes.REGISTER} />} />
					<AuthRoute isPublic name="Verification" path={routes.VERIFICATION} render={() => <Landing route={routes.VERIFICATION} />} />

					<AuthRoute isPrivate name="Home" path={routes.HOME} component={Home} />
					<AuthRoute isPrivate name="Article" path={routes.ARTICLE} component={Article} />
				</Switch>
				<Toasts />
			</>
		);
	}
}

const mapStateToProps = state => ({
	initializing: state.auth.initializing,
	initialized: state.auth.initialized,
});

const mapDispatchToProps = dispatch => ({
	initialize: () => dispatch(authActions.initialize()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Root);
