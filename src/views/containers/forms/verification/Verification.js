import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FormGroup, FormLabel, FormControl, FormText } from 'react-bootstrap';

import Button from '../../../components/button';

import { authActions } from '../../../../redux/auth';

import './Verification.scss';

const CLASS = 'fr-Verification';

class Verification extends Component {
	state = {
		code: '',
	};

	render() {
		return (
			<div className={CLASS}>
				<h2 className={`${CLASS}-title`}>Verify with Economique</h2>
				<div className={`${CLASS}-form`}>
					<FormGroup>
						<FormLabel>Verification code:</FormLabel>
						<FormControl type="text" placeholder="Enter verification code" onChange={e => this.setState({ code: e.target.value })} />
						<FormText className="text-muted">Enter the 6-digit code that was sent to {this.props.requestedEmail || 'your email'}</FormText>
					</FormGroup>
					<Button onClick={() => this.props.verification(this.state.code)}>Continue</Button>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	requestedEmail: state.auth.requestedEmail,
});

const mapDispatchToProps = dispatch => ({
	verification: code => dispatch(authActions.verification(code)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Verification);
