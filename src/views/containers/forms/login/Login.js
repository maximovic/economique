import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FormGroup, FormLabel, FormControl } from 'react-bootstrap';

import * as routes from '../../../../app/routes';

import Button from '../../../components/button';
import LinkedText from '../../../components/linked-text';

import validate from '../../../../validation';

import { authActions } from '../../../../redux/auth';

import './Login.scss';

const CLASS = 'fr-Login';

class Login extends Component {
	state = {
		email: '',
		password: '',
		errors: {
			email: null,
			password: null,
		},
	};

	submit = () => {
		const { email, password } = this.state;

		const emailValid = this.validateEmail(email);
		const passwordValid = this.validatePassword(password);

		return emailValid && passwordValid && this.props.signIn(email, password);
	};

	validateEmail = email => {
		try {
			validate(email, 'email');
			this.setState(prevState => ({
				errors: {
					...prevState.errors,
					email: null,
				},
			}));
			return true;
		} catch (error) {
			this.setState(prevState => ({
				errors: {
					...prevState.errors,
					email: error.message,
				},
			}));
			return false;
		}
	};

	validatePassword = password => {
		try {
			validate(password, 'password');
			this.setState(prevState => ({
				errors: {
					...prevState.errors,
					password: null,
				},
			}));
			return true;
		} catch (error) {
			this.setState(prevState => ({
				errors: {
					...prevState.errors,
					password: error.message,
				},
			}));
			return false;
		}
	};

	render() {
		return (
			<div className={CLASS}>
				<h2 className={`${CLASS}-title`}>Log in with Economique</h2>
				<div className={`${CLASS}-form`}>
					<FormGroup>
						<FormLabel>Email:</FormLabel>
						<FormControl
							className={this.state.errors.email ? `${CLASS}-input_err` : ''}
							type="email"
							placeholder="Enter email"
							onChange={e => this.setState({ email: e.target.value })}
						/>
						<span className={`${CLASS}-error`}>{this.state.errors.email || ''}</span>
					</FormGroup>
					<FormGroup>
						<FormLabel>Password:</FormLabel>
						<FormControl
							className={this.state.errors.password ? `${CLASS}-input_err` : ''}
							type="password"
							placeholder="Enter šifru"
							onChange={e => this.setState({ password: e.target.value })}
						/>
						<span className={`${CLASS}-error`}>{this.state.errors.password || ''}</span>
					</FormGroup>
					<Button onClick={this.submit}>Continue</Button>
					<LinkedText text="Don't have an account?" link="Register now" to={routes.REGISTER} />
				</div>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => ({
	signIn: (email, password) => dispatch(authActions.signIn(email, password)),
});

export default connect(null, mapDispatchToProps)(Login);
