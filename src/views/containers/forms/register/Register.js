import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FormGroup, FormLabel, FormControl } from 'react-bootstrap';

import * as routes from '../../../../app/routes';

import Button from '../../../components/button';
import LinkedText from '../../../components/linked-text';

import validate from '../../../../validation';

import { authActions } from '../../../../redux/auth';

import './Register.scss';

const CLASS = 'fr-Register';

class Register extends Component {
	state = {
		username: '',
		email: '',
		password: '',
		errors: {
			username: null,
			email: null,
			password: null,
		},
	};

	submit = () => {
		const { username, email, password } = this.state;

		const usernameValid = this.validateUsername(username);
		const emailValid = this.validateEmail(email);
		const passwordValid = this.validatePassword(password);

		return usernameValid && emailValid && passwordValid && this.props.register(username, email, password);
	};

	validateUsername = username => {
		try {
			validate(username, 'username');
			this.setState(prevState => ({
				errors: {
					...prevState.errors,
					username: null,
				},
			}));
			return true;
		} catch (error) {
			this.setState(prevState => ({
				errors: {
					...prevState.errors,
					username: error.message,
				},
			}));
			return false;
		}
	};

	validateEmail = email => {
		try {
			validate(email, 'email');
			this.setState(prevState => ({
				errors: {
					...prevState.errors,
					email: null,
				},
			}));
			return true;
		} catch (error) {
			this.setState(prevState => ({
				errors: {
					...prevState.errors,
					email: error.message,
				},
			}));
			return false;
		}
	};

	validatePassword = password => {
		try {
			validate(password, 'password');
			this.setState(prevState => ({
				errors: {
					...prevState.errors,
					password: null,
				},
			}));
			return true;
		} catch (error) {
			this.setState(prevState => ({
				errors: {
					...prevState.errors,
					password: error.message,
				},
			}));
			return false;
		}
	};

	render() {
		return (
			<div className={CLASS}>
				<h2 className={`${CLASS}-title`}>Register with Economique</h2>
				<div className={`${CLASS}-form`}>
					<FormGroup>
						<FormLabel>Username:</FormLabel>
						<FormControl
							className={this.state.errors.username ? `${CLASS}-input_err` : ''}
							type="text"
							placeholder="Enter username"
							onChange={e => this.setState({ username: e.target.value })}
						/>
						<span className={`${CLASS}-error`}>{this.state.errors.username || ''}</span>
					</FormGroup>
					<FormGroup>
						<FormLabel>Email:</FormLabel>
						<FormControl
							className={this.state.errors.email ? `${CLASS}-input_err` : ''}
							type="email"
							placeholder="Enter email"
							onChange={e => this.setState({ email: e.target.value })}
						/>
						<span className={`${CLASS}-error`}>{this.state.errors.email || ''}</span>
					</FormGroup>
					<FormGroup>
						<FormLabel>Password:</FormLabel>
						<FormControl
							className={this.state.errors.password ? `${CLASS}-input_err` : ''}
							type="password"
							placeholder="Enter password"
							onChange={e => this.setState({ password: e.target.value })}
						/>
						<span className={`${CLASS}-error`}>{this.state.errors.password || ''}</span>
					</FormGroup>
					<Button onClick={this.submit}>Create account</Button>
					<LinkedText text="Have an account?" link="Log in here" to={routes.LANDING} />
				</div>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => ({
	register: (username, email, password) => dispatch(authActions.register(username, email, password)),
});

export default connect(null, mapDispatchToProps)(Register);
