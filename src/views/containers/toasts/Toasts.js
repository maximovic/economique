import React from 'react';
import { connect } from 'react-redux';

import Toast from '../../components/toast';

import { toastsActions } from '../../../redux/toasts';

import './Toasts.scss';

const CLASS = 'gt-Toasts';

const Toasts = props => {
	const renderToasts = () => {
		if (!props.toasts) return null;
		return props.toasts.map(toast => {
			return <Toast key={`Toast-${toast}`} message={toast} onRemove={message => props.removeToast(message)} />;
		});
	};
	return <div className={CLASS}>{renderToasts()}</div>;
};

const mapStateToProps = state => ({
	toasts: state.toasts.messages,
});

const mapDispatchToProps = dispatch => ({
	removeToast: message => dispatch(toastsActions.removeToast(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Toasts);
