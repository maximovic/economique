import React, { Component } from 'react';
import { connect } from 'react-redux';

import Spinner from '../../components/spinner';

import { articlesActions } from '../../../redux/articles';

import './Article.scss';

const CLASS = 'fr-Article';

class Article extends Component {
	componentDidMount() {
		this.props.getArticle(this.props.match.params.url);
	}

	renderArticle = () => {
		const { article } = this.props;

		if (!article) {
			return null;
		}

		return (
			<>
				<span className={`${CLASS}-date`}>{article.date}</span>
				<h2 className={`${CLASS}-title`}>{article.title}</h2>
				<p className={`${CLASS}-descrition`}>{article.description}</p>
				<img className={`${CLASS}-image`} src={article.image} alt="article" />
				{article.texts.map(text => (
					<p key={text} className={`${CLASS}-text`}>
						{text}
					</p>
				))}
			</>
		);
	};

	renderArticleOrSpinner = () => {
		if (this.props.loading) {
			return <Spinner size={150} />;
		}

		return this.renderArticle();
	};

	render() {
		return <div className={CLASS}>{this.renderArticleOrSpinner()}</div>;
	}
}

const mapStateToProps = state => ({
	loading: state.articles.loading,
	article: state.articles.article,
});

const mapDispatchToProps = dispatch => ({
	getArticle: url => dispatch(articlesActions.getArticle(url)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Article);
