import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Spinner from '../../components/spinner';
import Teaser from '../../components/teaser';

import { articleRoute } from '../../../app/routes';

import { articlesActions } from '../../../redux/articles';

import './Home.scss';

const CLASS = 'fr-Home';

class Home extends Component {
	componentDidMount() {
		this.props.getArticles();
	}

	renderArticles = () => {
		if (!this.props.articles) {
			return null;
		}

		return this.props.articles.map(article => {
			return (
				<Link className={`${CLASS}-link`} key={article.title} to={articleRoute(article.url)}>
					<Teaser title={article.title} text={article.text} image={article.image} />
				</Link>
			);
		});
	};

	renderHomeOrSpinner = () => {
		if (this.props.loading) {
			return <Spinner size={150} />;
		}

		return (
			<>
				<h2 className={`${CLASS}-title`}>Top stories</h2>
				<div className={`${CLASS}-list`}>{this.renderArticles()}</div>
			</>
		);
	};

	render() {
		return <div className={CLASS}>{this.renderHomeOrSpinner()}</div>;
	}
}

const mapStateToProps = state => ({
	loading: state.articles.loading,
	articles: state.articles.articles,
});

const mapDispatchToProps = dispatch => ({
	getArticles: () => dispatch(articlesActions.getArticles()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
