import React from 'react';

import Login from '../../containers/forms/login';
import Register from '../../containers/forms/register';
import Verification from '../../containers/forms/verification';

import * as routes from '../../../app/routes';

import './Landing.scss';

const CLASS = 'fr-Landing';

const Landing = props => {
	const renderForm = () => {
		switch (props.route) {
			case routes.LANDING:
				return <Login />;
			case routes.REGISTER:
				return <Register />;
			case routes.VERIFICATION:
				return <Verification />;
			default:
				return null;
		}
	};

	return <div className={CLASS}>{renderForm()}</div>;
};

export default Landing;
