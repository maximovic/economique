import React from 'react';

import './Teaser.scss';

const CLASS = 'fr-Teaser';

function Teaser(props) {
	return (
		<div className={CLASS}>
			<div>
				<h3 className={`${CLASS}-title`}>{props.title}</h3>
				{props.text && <p className={`${CLASS}-text`}>{props.text}</p>}
			</div>
			{props.image && <img className={`${CLASS}-image`} src={props.image} alt="teaser" />}
		</div>
	);
}

export default Teaser;
