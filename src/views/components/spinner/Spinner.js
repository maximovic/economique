import React from 'react';

import './Spinner.scss';

const CLASS = 'gt-Spinner';

const Spinner = props => {
	const styles = {
		wrapper: {
			width: props.fullscreen ? '100vw' : 'auto',
			height: props.fullscreen ? '100vh' : 'auto',
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
			position: 'absolute',
			top: 0,
			left: 0,
		},
		spinner: {
			width: `${props.size}px`,
			height: `${props.size}px`,
		},
	};

	return props.fullscreen ? (
		<div style={styles.wrapper}>
			<div style={styles.spinner} className={CLASS} />
		</div>
	) : (
		<div style={styles.spinner} className={CLASS} />
	);
};

export default Spinner;
