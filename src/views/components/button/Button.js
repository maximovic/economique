import React from 'react';

import './Button.scss';

const CLASS = 'fr-Button';

function Button(props) {
	return (
		<button className={`${CLASS} ${CLASS}_${props.variant || 'primary'}`} onClick={props.onClick} disabled={props.disabled}>
			{props.children}
		</button>
	);
}

Button.defaultProps = {
	variant: 'primary',
};

export default Button;
