import React from 'react';

import { Link } from 'react-router-dom';

import './LinkedText.scss';

const CLASS = 'fr-LinkedText';

const LinkedText = props => {
	const { text, link, to } = props;
	return (
		<div className={CLASS}>
			<span>{text}</span>
			<Link to={to}>{link}</Link>
		</div>
	);
};

export default LinkedText;
