import React from 'react';

import close from '../../../assets/close-min.png';

import './Toast.scss';

const CLASS = 'gt-Toast';

const DEFAULT_TIMEOUT = 3000;

const Toast = props => {
	let defaultTimeout;

	const addRemove = toast => toast.classList.add(`${CLASS}-remove`);

	const onLoad = event => {
		const { currentTarget: toast } = event;
		toast.addEventListener('animationend', event => event.animationName === 'fade-out-right' && props.onRemove(props.message));
		defaultTimeout = setTimeout(() => addRemove(toast), DEFAULT_TIMEOUT);
	};

	const onRemove = event => {
		const { parentNode: toast } = event.target;
		clearTimeout(defaultTimeout);
		addRemove(toast);
	};

	return (
		<div className={CLASS} onLoad={event => onLoad(event)}>
			<span>{props.message}</span>
			<img width={12} src={close} onClick={event => onRemove(event)} alt="remove toast" />
		</div>
	);
};

export default Toast;
