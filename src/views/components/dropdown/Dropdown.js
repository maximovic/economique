import React from 'react';
import { Dropdown } from 'react-bootstrap';
import { CaretDownFill } from 'react-bootstrap-icons';

import './Dropdown.scss';

const CLASS = 'fr-Dropdown';

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
	<span
		className={`${CLASS}-toggle`}
		ref={ref}
		onClick={e => {
			e.preventDefault();
			onClick(e);
		}}
	>
		<span>{children}</span>
		<CaretDownFill size={18} />
	</span>
));

CustomToggle.displayName = 'CustomToggle';

function CustomDropdown(props) {
	const renderOptions = () => {
		if (!props.options) {
			return null;
		}

		return props.options.map((option, index) => {
			return (
				<Dropdown.Item as="span" key={index} eventKey={index}>
					{option}
				</Dropdown.Item>
			);
		});
	};

	return (
		<Dropdown>
			<Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
				{props.label}
			</Dropdown.Toggle>

			<Dropdown.Menu className={`${CLASS}-menu`}>{renderOptions()}</Dropdown.Menu>
		</Dropdown>
	);
}

export default CustomDropdown;
