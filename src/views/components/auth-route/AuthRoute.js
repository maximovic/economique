import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import * as routes from '../../../app/routes';

const AuthRoute = props => {
	const { isPrivate = false, isPublic = false, user, render, ...settings } = props;

	const redirectAuth = <Redirect to={routes.LANDING} />;
	const renderComponent = <Route {...settings} render={render} />;

	if (isPrivate) {
		return user ? renderComponent : redirectAuth;
	} else if (isPublic) {
		return renderComponent;
	} else {
		return renderComponent;
	}
};

const mapStateToProps = state => ({
	user: state.auth.user,
});

export default connect(mapStateToProps, null)(AuthRoute);
