import React from 'react';
import { connect } from 'react-redux';
import { Link, NavLink } from 'react-router-dom';

import CustomDropdown from '../dropdown';

import * as routes from '../../../app/routes';

import { authActions } from '../../../redux/auth';

import './Header.scss';

const CLASS = 'gt-Header';

const Header = props => {
	const renderAuthLinks = () => {
		if (props.authenticated) {
			return (
				<>
					<CustomDropdown
						label={props.username}
						options={[
							<span key={3} className={`${CLASS}-dropdown`} onClick={props.signOut}>
								Log out
							</span>,
						]}
					/>
				</>
			);
		}
		return (
			<>
				<NavLink className={`${CLASS}-link`} activeClassName={`${CLASS}-link_active`} to={routes.LANDING}>
					Login
				</NavLink>
				<NavLink exact className={`${CLASS}-link`} activeClassName={`${CLASS}-link_active`} to={routes.REGISTER}>
					Register
				</NavLink>
			</>
		);
	};

	return (
		<div className={`${CLASS}`}>
			<Link className={`${CLASS}-logo`} to={routes.HOME}>
				Economique
			</Link>
			<div className={`${CLASS}-nav`}>{renderAuthLinks()}</div>
		</div>
	);
};

const mapStateToProps = state => ({
	authenticated: state.auth.authenticated,
	username: state.auth.user?.username,
});

const mapDispatchToProps = disptach => ({
	signOut: () => disptach(authActions.signOut()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
