export const names = {
	principal: 'principal',
};

export const get = name => {
	const item = localStorage.getItem(name);
	return item ? JSON.parse(item) : null;
};

export const set = (name, value) => {
	const item = localStorage.setItem(name, value ? JSON.stringify(value) : null);
	return !!item;
};

export const remove = name => {
	const item = localStorage.removeItem(name);
	return !item;
};
