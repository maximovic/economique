// ***** PUBLIC ROUTES *****

export const LANDING = '/';
export const REGISTER = '/register';
export const VERIFICATION = '/verification';

// ***** PRIVATE ROUTES *****

export const HOME = '/home';
export const ARTICLE = '/article/:url+';
export const articleRoute = url => `/article/${url.substring(1)}`;
