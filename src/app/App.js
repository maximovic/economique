import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

import initStore from '../redux/store';
import history from './history';
import Root from '../views/routers/root';

const store = initStore();

const App = () => {
	return (
		<Provider store={store}>
			<ConnectedRouter history={history}>
				<Root />
			</ConnectedRouter>
		</Provider>
	);
};

export default App;
