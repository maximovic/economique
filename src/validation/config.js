const formats = {
	required: /\S/,
	email: /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i,
};

export const messages = {
	required: `This field is required`,
	email: 'Invalid email address',
};

const rules = {
	username: {
		required: {
			message: messages.required,
			format: formats.required,
		},
	},
	email: {
		required: {
			message: messages.required,
			format: formats.required,
		},
		invalid: {
			message: messages.email,
			format: formats.email,
		},
	},
	password: {
		required: {
			message: messages.required,
			format: formats.required,
		},
	},
};

export default rules;
