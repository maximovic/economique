// @flow
import config from './config';

const validate = (value: string, field: string) => {
	const rules = Object.values(config[field]);
	rules.forEach((rule: Object) => {
		if (!value.match(rule.format)) {
			throw new Error(rule.message);
		}
	});
};

export default validate;
