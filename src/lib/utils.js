export function shuffle(arr) {
	const newArr = [...arr];

	for (let i = newArr.length - 1; i > 0; i--) {
		let j = Math.floor(Math.random() * (i + 1));
		[newArr[i], newArr[j]] = [newArr[j], newArr[i]];
	}

	return newArr;
}

export function isInView(element) {
	const bounding = element.getBoundingClientRect();
	return bounding.x > 0 && bounding.x < window.innerWidth;
}

export function isInMiddle(element) {
	const bounding = element.getBoundingClientRect();
	const item = bounding.width + 4;
	const middle = window.innerWidth / 2;

	return bounding.x >= middle - item && bounding.x <= middle;
}

export function getMiddlePosition(element) {
	const bounding = element.getBoundingClientRect();
	const item = bounding.width + 4;
	const middle = window.innerWidth / 2;

	return middle - item / 2 - bounding.x;
}

export function isNegative(num) {
	return !~Math.sign(num);
}

export function numFromStr(str) {
	return parseFloat(str.match(/(\d+)/g)[0]);
}
