import axios from 'axios';
import * as storage from '../util/storage';

export const getHeaders = (method, onlyAuth = false) => {
	const principal = storage.get(storage.names.principal);

	let headers = {
		Authorization: `Bearer ${principal && principal.session && principal.session.access_token ? principal.session.access_token : undefined}`,
	};

	if (onlyAuth) {
		return headers;
	}

	if (method === 'post' || method === 'put') {
		headers['Content-Type'] = 'application/json';
	}

	return headers;
};

export const request = opts => {
	if (!opts.url) {
		throw new Error('url is required');
	}
	
	opts.baseURL = opts.baseURL || 'http://localhost:8000';
	opts.method = opts.method || 'get';
	opts.headers = opts.headers || getHeaders(opts.method);

	return axios(opts)
		.then(res => {
			return res.data;
		})
		.catch(res => {
			let err = null;
			let response = res.response;
			if (response && response.data && response.data.message) {
				err = response.data;
			} else if (response) {
				err = new Error(response.statusText);
				err.status = response.status;
			} else {
				err = new Error(res.message || 'HTTP Error');
				err.status = 0;
			}

			throw err;
		});
};

export const get = (url, params) => request({ url, params });

export const post = (url, params, data) => request({ method: 'post', url, params, data });

export const put = (url, params, data) => request({ method: 'put', url, params, data });

export const del = (url, params, data) => request({ method: 'delete', url, params, data });
