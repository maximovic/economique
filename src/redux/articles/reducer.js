import * as TYPES from './action-types';

const INITAL_STATE = {
	loading: false,
	error: null,
	articles: null,
	article: null,
};

// ***** GET ARTICLES *****

export const getArticlesStart = state => {
	return {
		...state,
		loading: true,
		error: null,
	};
};

export const getArticlesError = (state, error) => {
	return {
		...state,
		loading: false,
		error,
	};
};

export const getArticlesEnd = (state, articles) => {
	return {
		...state,
		loading: false,
		articles,
	};
};

// ***** GET ARTICLE *****

export const getArticleStart = state => {
	return {
		...state,
		loading: true,
		error: null,
	};
};

export const getArticleError = (state, error) => {
	return {
		...state,
		loading: false,
		error,
	};
};

export const getArticleEnd = (state, article) => {
	return {
		...state,
		loading: false,
		article,
	};
};

export const articlesReducer = (state = INITAL_STATE, { type, payload }) => {
	switch (type) {
		// ***** GET ARTICLES *****
		case TYPES.GET_ARTICLES_START:
			return getArticlesStart(state);
		case TYPES.GET_ARTICLES_ERROR:
			return getArticlesError(state, payload);
		case TYPES.GET_ARTICLES_END:
			return getArticlesEnd(state, payload);

		// ***** GET ARTICLE *****
		case TYPES.GET_ARTICLE_START:
			return getArticleStart(state);
		case TYPES.GET_ARTICLE_ERROR:
			return getArticleError(state, payload);
		case TYPES.GET_ARTICLE_END:
			return getArticleEnd(state, payload);

		default:
			return state;
	}
};
