import * as TYPES from './action-types';

import * as http from '../../lib/http';

// ***** GET ARTICLES *****

export const getArticlesStart = () => {
	return {
		type: TYPES.GET_ARTICLES_START,
	};
};

export const getArticlesError = error => {
	return {
		type: TYPES.GET_ARTICLES_ERROR,
		payload: error,
	};
};

export const getArticlesEnd = payload => {
	return {
		type: TYPES.GET_ARTICLES_END,
		payload,
	};
};

export const getArticles = () => {
	return dispatch => {
		dispatch(getArticlesStart());
		http
			.get('scrape/homepage')
			.then(res => {
				dispatch(getArticlesEnd(res));
			})
			.catch(error => {
				dispatch(getArticlesError(error));
			});
	};
};

// ***** GET ARTICLE *****

export const getArticleStart = () => {
	return {
		type: TYPES.GET_ARTICLE_START,
	};
};

export const getArticleError = error => {
	return {
		type: TYPES.GET_ARTICLE_ERROR,
		payload: error,
	};
};

export const getArticleEnd = payload => {
	return {
		type: TYPES.GET_ARTICLE_END,
		payload,
	};
};

export const getArticle = url => {
	return dispatch => {
		dispatch(getArticleStart());
		http
			.get('scrape/article/' + url)
			.then(res => {
				dispatch(getArticleEnd(res));
			})
			.catch(error => {
				dispatch(getArticleError(error));
			});
	};
};
