import * as TYPES from './action-types';

import history from '../../app/history';
import * as routes from '../../app/routes';

import * as http from '../../lib/http';
import * as storage from '../../util/storage';

import { toastsActions } from '../toasts';

// ***** MESSAGES *****

const toastMessages = {
	signIn: 'Hello 👋',
	verify: 'Welcome  🎉',
	signOut: 'Bye bye 👋',
	error: message => `${message} 🛑`,
};

// ***** INIT *****

export const initStart = () => {
	return {
		type: TYPES.INIT_START,
	};
};

export const initError = error => {
	return {
		type: TYPES.INIT_ERROR,
		payload: error,
	};
};

export const initEnd = uid => {
	return {
		type: TYPES.INIT_END,
		payload: uid,
	};
};

export const initialize = () => {
	return dispatch => {
		dispatch(initStart());
		try {
			const principal = storage.get(storage.names.principal);
			if (principal) {
				dispatch(toastsActions.addToast(toastMessages.signIn));
				history.push(routes.HOME);
			}
			dispatch(initEnd(principal));
		} catch (error) {
			dispatch(toastsActions.addToast(toastMessages.error(error.message)));
			dispatch(initError(error));
		}
	};
};

// ***** SIGN IN *****

export const signInStart = () => {
	return {
		type: TYPES.SIGN_IN_START,
	};
};

export const signInError = error => {
	return {
		type: TYPES.SIGN_IN_ERROR,
		payload: error,
	};
};

export const signInEnd = user => {
	return {
		type: TYPES.SIGN_IN_END,
		payload: user,
	};
};

export const signIn = (email, password) => {
	return dispatch => {
		dispatch(signInStart());
		http
			.post('auth/login', null, { email, password })
			.then(principal => {
				storage.set(storage.names.principal, principal);
				dispatch(signInEnd(principal));
				dispatch(toastsActions.addToast(toastMessages.signIn));
				history.push(routes.HOME);
			})
			.catch(error => {
				dispatch(toastsActions.addToast(toastMessages.error(error.message)));
				dispatch(signInError(error));
			});
	};
};

// ***** REGISTER *****

export const registerStart = () => {
	return {
		type: TYPES.REGISTER_START,
	};
};

export const registerError = error => {
	return {
		type: TYPES.REGISTER_ERROR,
		payload: error,
	};
};

export const registerEnd = email => {
	return {
		type: TYPES.REGISTER_END,
		payload: email,
	};
};

export const register = (username, email, password) => {
	return dispatch => {
		dispatch(registerStart());
		http
			.post('auth/register', null, { username, email, password })
			.then(() => {
				dispatch(registerEnd(email));
				history.push(routes.VERIFICATION);
			})
			.catch(error => {
				dispatch(toastsActions.addToast(toastMessages.error(error.message)));
				dispatch(registerError(error));
			});
	};
};

// ***** VERIFICATION *****

export const verificationStart = () => {
	return {
		type: TYPES.VERIFICATION_START,
	};
};

export const verificationError = error => {
	return {
		type: TYPES.VERIFICATION_ERROR,
		payload: error,
	};
};

export const verificationEnd = principal => {
	return {
		type: TYPES.VERIFICATION_END,
		payload: principal,
	};
};

export const verification = code => {
	return (dispatch, getState) => {
		const email = getState().auth.requestedEmail;
		if (!email) {
			return;
		}
		dispatch(verificationStart());
		http
			.put('auth/verification', null, { email, verification_code: code })
			.then(principal => {
				storage.set(storage.names.principal, principal);
				dispatch(verificationEnd(principal));
				dispatch(toastsActions.addToast(toastMessages.verify));
				history.push(routes.HOME);
			})
			.catch(error => {
				dispatch(toastsActions.addToast(toastMessages.error(error.message)));
				dispatch(verificationError(error));
			});
	};
};

// ***** SIGN OUT *****

export const signOutStart = () => {
	return {
		type: TYPES.SIGN_OUT_START,
	};
};

export const signOutError = error => {
	return {
		type: TYPES.SIGN_OUT_ERROR,
		payload: error,
	};
};

export const signOutEnd = () => {
	return {
		type: TYPES.SIGN_OUT_END,
	};
};

export const signOut = () => {
	return dispatch => {
		dispatch(signOutStart());
		try {
			storage.remove(storage.names.principal);
			dispatch(signOutEnd());
			history.push(routes.HOME);
			dispatch(toastsActions.addToast(toastMessages.signOut));
		} catch (error) {
			dispatch(toastsActions.addToast(toastMessages.error(error.message)));
			dispatch(signOutError(error));
		}
	};
};
