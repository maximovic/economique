import * as TYPES from './action-types';

const INITAL_STATE = {
	initializing: false,
	initialized: false,
	loading: false,
	error: null,
	authenticated: false,
	user: null,
	session: null,
	requestedEmail: null,
};

// ***** INIT *****

export const initStart = state => {
	return {
		...state,
		initializing: true,
		error: null,
	};
};

export const initError = (state, error) => {
	return {
		...state,
		initializing: false,
		initialized: false,
		authenticated: false,
		token: null,
		error,
	};
};

export const initEnd = (state, principal) => {
	return {
		...state,
		initializing: false,
		initialized: true,
		authenticated: !!principal,
		user: principal?.user,
		session: principal?.session,
		error: null,
	};
};

// ***** SIGN IN *****

export const signInStart = state => {
	return {
		...state,
		loading: true,
		error: null,
	};
};

export const signInError = (state, error) => {
	return {
		...state,
		loading: false,
		authenticated: false,
		user: null,
		session: null,
		error,
	};
};

export const signInEnd = (state, { user, session }) => {
	return {
		...state,
		loading: false,
		authenticated: true,
		error: null,
		user,
		session,
	};
};

// ***** REGISTER *****

export const registerStart = state => {
	return {
		...state,
		loading: true,
		error: null,
	};
};

export const registerError = (state, error) => {
	return {
		...state,
		loading: false,
		authenticated: false,
		user: null,
		error,
	};
};

export const registerEnd = (state, email) => {
	return {
		...state,
		loading: false,
		error: null,
		requestedEmail: email,
	};
};

// ***** VERIFY *****

export const verifyStart = state => {
	return {
		...state,
		loading: true,
		error: null,
	};
};

export const verifyError = (state, error) => {
	return {
		...state,
		loading: false,
		authenticated: false,
		user: null,
		error,
	};
};

export const verifyEnd = (state, { user, session }) => {
	return {
		...state,
		loading: false,
		authenticated: true,
		error: null,
		user,
		session,
	};
};

// ***** SIGN OUT *****

export const signOutStart = state => {
	return {
		...state,
		loading: true,
		error: null,
	};
};

export const signOutError = (state, error) => {
	return {
		...state,
		loading: false,
		error,
	};
};

export const signOutEnd = state => {
	return {
		...state,
		loading: false,
		authenticated: false,
		user: null,
		error: null,
	};
};

export const clearError = state => {
	return {
		...state,
		error: null,
	};
};

export const authReducer = (state = INITAL_STATE, { type, payload }) => {
	switch (type) {
		// ***** INIT *****
		case TYPES.INIT_START:
			return initStart(state);
		case TYPES.INIT_ERROR:
			return initError(state, payload);
		case TYPES.INIT_END:
			return initEnd(state, payload);

		// ***** SIGN IN *****
		case TYPES.SIGN_IN_START:
			return signInStart(state);
		case TYPES.SIGN_IN_ERROR:
			return signInError(state, payload);
		case TYPES.SIGN_IN_END:
			return signInEnd(state, payload);

		// ***** REGISTER *****
		case TYPES.REGISTER_START:
			return registerStart(state);
		case TYPES.REGISTER_ERROR:
			return registerError(state, payload);
		case TYPES.REGISTER_END:
			return registerEnd(state, payload);

		// ***** VERIFICATION *****
		case TYPES.VERIFICATION_START:
			return verifyStart(state);
		case TYPES.VERIFICATION_ERROR:
			return verifyError(state, payload);
		case TYPES.VERIFICATION_END:
			return verifyEnd(state, payload);

		// ***** SIGN OUT *****
		case TYPES.SIGN_OUT_START:
			return signOutStart(state);
		case TYPES.SIGN_OUT_ERROR:
			return signOutError(state, payload);
		case TYPES.SIGN_OUT_END:
			return signOutEnd(state);

		case TYPES.ROUTER_LOCATION_CHANGE:
			return clearError(state);

		default:
			return state;
	}
};
