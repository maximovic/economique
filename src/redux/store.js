import { createStore, compose, applyMiddleware } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';

import history from '../app/history';
import reducers from './reducers';

const initStore = initialState => {
	const middlewares = [thunk, routerMiddleware(history)];

	if (process.env.NODE_ENV === 'development') {
		const { logger } = require('redux-logger');

		middlewares.push(logger);
	}

	const middleware = compose(applyMiddleware(...middlewares));
	const store = createStore(reducers(history), initialState, middleware);

	return store;
};

export default initStore;
