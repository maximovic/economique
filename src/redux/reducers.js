import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import { authReducer } from './auth';
import { toastsReducer } from './toasts';
import { articlesReducer } from './articles';

const rootReducer = history =>
	combineReducers({
		router: connectRouter(history),
		auth: authReducer,
		toasts: toastsReducer,
		articles: articlesReducer,
	});

export default rootReducer;
