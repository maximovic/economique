import * as TYPES from './action-types';

export const addToast = toast => {
	return dispatch => {
		dispatch({
			type: TYPES.ADD_TOAST,
			payload: toast,
		});
	};
};

export const removeToast = toast => {
	return dispatch => {
		dispatch({
			type: TYPES.REMOVE_TOAST,
			payload: toast,
		});
	};
};
