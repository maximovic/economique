// @flow
import * as TYPES from './action-types';

const INITIAL_STATE = {
	messages: null,
};

const addToast = (state, toast) => {
	return {
		messages: state.messages ? [...state.messages, toast] : [toast],
	};
};

const removeToast = (state, toast) => {
	return {
		messages: state.messages ? (state.messages.includes(toast) ? state.messages.filter(item => item !== toast) : state.messages) : null,
	};
};

export const toastsReducer = (state = INITIAL_STATE, { type, payload }) => {
	switch (type) {
		case TYPES.ADD_TOAST:
			return addToast(state, payload);
		case TYPES.REMOVE_TOAST:
			return removeToast(state, payload);
		default:
			return state;
	}
};
