# Economique

## How to run:

1. Clone the repo `git clone https://gitlab.com/maximovic/economique.git`

2. Navigate to the project directory `cd ./economique`

3. Run `npm install` to install all the dependencies.

4. Run `npm start`.